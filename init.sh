#!/bin/bash
echo "*****************************"
echo "*   Init the emacs config   *"
echo "*****************************"
echo "-> create the default virtual opython environment"
sudo apt-get install python-virtualenv
mkdir -p  ~/.emacs.d/.python-environments/default
virtualenv ~/.emacs.d/.python-environments/default
echo "-> backup the exsisting ~/.emacs file if it exsist"
mv ~/.emacs ~/.emacs.d/origin_emacs
echo "Copy all files to ~/emacs.d.."

cp -rv ./ ~/.emacs.d

echo "-> using the implicit version"

cp -f ~/.emacs.d/emacs_start ~/.emacs

echo "python elpy setup :-) "

sudo pip install  jedi

sudo pip install  importmagic

sudo pip install flake8
