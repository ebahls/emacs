@echo off
echo "**********************"
echo "Install Emacs Config"
echo "**********************"

set emacs=%USERPROFILE%\AppData\Roaming\

echo %emacs%

rd /S %emacs%\.emacs.d

 del %emacs%\.emacs

cd 
mkdir %emacs%\.emacs.d
mkdir %emacs%\.emacs.d\.python-environment
mkdir %emacs%\.emacs.d\.python-environment\default

rem pip install virtualenv

rem virtualenv %emacs%\.emacs.d\.python-environment\default

xcopy %cd% %emacs%.emacs.d /s

copy emacs_start %emacs%\.emacs.d
